package com.automation.demo.stepDefs;

import com.automation.demo.Pages.FrontPage;
import cucumber.api.java.en.Given;
import org.springframework.beans.factory.annotation.Autowired;

public class MyStepdefs {
    @Autowired
    FrontPage frontPage;

    @Given("^I am on google home page$")
    public void iAmOnGoogleHomePage() {
       frontPage.goToHomePage("Google");
    }
}
