package com.automation.demo.stepDefs;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CucumberHooks {

    @Before
    public void beforeScenario(Scenario scenario) {
        log.info("Starting - " + scenario.getName());
    }

    @After
    public void afterScenario(Scenario scenario) {
        log.info(scenario.getName() + " Status - " + scenario.getStatus());
    }
}

