package com.automation.demo.featureRunnerFiles;

/**
 * Created by swapnali bhansali
 */

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)

@CucumberOptions(
        features = {"src/test/resources/com.automation.demo/SampleAutomation.feature"},
        strict = true,
        glue = {"com.automation.demo"},
        monochrome = true,
        plugin = {"pretty", "html:target/cucumber-reports/"}
        // tags = {"@registration and not @ignore and not @performance"}
)

public class TestRunner {
}
