package com.automation.demo.helpers;

public class DomainHelper {
    public static String getDomainName(String env) {

        //env = System.getProperty("env");
        switch (env) {
            case "qa":
                return "http://google.com";
            case "dev":
                return "http://amazon.com";
            default:
                return "http://yahoo.com";
        }
    }
}
