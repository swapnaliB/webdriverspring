package com.automation.demo.helpers;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

@Slf4j
public class WebDriverFactory {
    private WebDriver webDriver;

    public WebDriver getWebDriver(){
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/chromeDriver/mac/chromedriver");
        webDriver = new ChromeDriver();
        log.info("Webdriver created");
        return webDriver;
    }

    public void tearDown(){
       webDriver.quit();
    }
}
