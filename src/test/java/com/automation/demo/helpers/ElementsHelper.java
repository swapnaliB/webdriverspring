package com.automation.demo.helpers;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * Created by swapnali bhansali
 */
@Component
@Slf4j
public class ElementsHelper {
    @Autowired
    private WebDriver webDriver;

    public boolean visibility(By byId) {
        try {
            WebElement ele = (new WebDriverWait(webDriver, 10))
                    .until(ExpectedConditions.visibilityOfElementLocated(byId));
            if (ele != null) {
                return true;
            }

        } catch (Exception e) {
            log.info("Venue info not found");
            return false;
        }
        return false;
    }

    public boolean clickability(By byId) {
        try {
            WebElement ele = (new WebDriverWait(webDriver, 10))
                    .until(ExpectedConditions.elementToBeClickable(byId));
            if (ele != null) {
                return true;
            }

        } catch (Exception e) {
            log.info("Venue info not found");
            return false;
        }
        return false;
    }

    public boolean waitForRefresh(By byId) {
        try {
            WebElement ele = (new WebDriverWait(webDriver, 10))
                    .until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOfElementLocated(byId)));
            if (ele != null) {
                return true;
            }

        } catch (Exception e) {
            log.info("Venue info not found");
            return false;
        }
        return false;
    }

    public boolean visibility(By byId, int timeout) {
        try {
            WebElement venue = (new WebDriverWait(webDriver, timeout))
                    .until(ExpectedConditions.visibilityOfElementLocated(byId));
            if (venue != null) {
                return true;
            }

        } catch (Exception e) {
            log.info("element not visible");
            return false;
        }
        return false;
    }

    public boolean checkText(By byId, String text) {
        try {
            WebElement ele = (new WebDriverWait(webDriver, 10))
                    .until(ExpectedConditions.presenceOfElementLocated(byId));
            if (ele != null) {
                if (ele.getText().equalsIgnoreCase(text)) {

                    return true;
                }
            }

        } catch (Exception e) {
            log.info("\n" + text + " not found");
            return false;
        }
        return false;
    }

    public boolean checkText(By byId, String text, int timeout) {

        try {
            WebElement ele = (new WebDriverWait(webDriver, timeout))
                    .until(ExpectedConditions.presenceOfElementLocated(byId));
            if (ele != null) {
                log.info(ele.getText());
                if (ele.getText().equalsIgnoreCase(text)) {

                    return true;
                }
            }

        } catch (Exception e) {
            log.info("\n" + text + " not found");
            return false;
        }
        return false;
    }

    public boolean containsText(By byId, String text) {
        try {
            WebElement ele = (new WebDriverWait(webDriver, 10))
                    .until(ExpectedConditions.visibilityOfElementLocated(byId));
            if (ele != null) {
                log.info(ele.getText());

                if (ele.getText().contains(text)) {
                    return true;
                }
            }

        } catch (Exception e) {
            log.info("\n" + text + " not found");
            return false;
        }
        return false;
    }

    public boolean clickOnEle(By byId) {
        try {
            WebElement venue = (new WebDriverWait(webDriver, 10))
                    .until(ExpectedConditions.visibilityOfElementLocated(byId));
            if (venue != null) {
                venue.click();
            }

        } catch (Exception e) {
            log.info("elemnent to click not found");
            return false;
        }
        return false;
    }

    public boolean isNewWindowOpen(String title, String partUrl, String paymentPageWindowHandle) {
        try {
            Thread.sleep(4000);
        } catch (Exception e) {
            log.info("sleep interrupted");
        }

        Set<String> windowTitles = webDriver.getWindowHandles();
        String newWindowHandle = null;
        for (String handle : windowTitles) {
            if (!handle.equals(paymentPageWindowHandle)) {
                webDriver.switchTo().window(handle);
                (new WebDriverWait(webDriver, 60))
                        .until(ExpectedConditions.titleContains(title));
                log.info("webDriver.getCurrentUrl()" + webDriver.getCurrentUrl());
                log.info("partUrl" + partUrl);


                if (webDriver.getCurrentUrl().toLowerCase().contains(partUrl.toLowerCase())) {
                    webDriver.close();
                    webDriver.switchTo().window(paymentPageWindowHandle);
                    return true;
                }
            }
        }
        webDriver.switchTo().window(paymentPageWindowHandle);
        return false;
    }


    public boolean checkIfMetaTagPresent(String tagName) {
        return webDriver.getPageSource().contains(tagName);
    }

    /*  public boolean checkMetaTagValue(String s, String tagName, String value) {
           By bytagSelector = By.cssSelector(tagName);
          try{
             //  WebElement ele = (new WebDriverWait(webDriver, 10))
               //   .until(ExpectedConditions.presenceOfElementLocated(bytagSelector));
              WebElement ele =   webDriver.findElement(bytagSelector);
              if(ele!=null){
                  log.infoln(ele.getText());
                  if(ele.getText().equals(value)){
                      return true;
                  }
              }

          }catch(Exception e) {
              log.info( "\n" + value + " not found");
              return false;
          }
          return false;

      }   */
    public WebElement getMatchingEleFromList(By byId, String showName) {
        List<WebElement> resultEle = (new WebDriverWait(webDriver, 10))
                .until(ExpectedConditions.visibilityOfAllElementsLocatedBy(byId));
        for (WebElement aResultEle : resultEle) {
            if (aResultEle.getText().contains(showName)) {
                return aResultEle;
            }
        }
        return null;

    }

    public boolean checkMetaTagValue(String tagName, String attribute, String value) {
        By bytagSelector = new By.ByCssSelector(tagName);
        try {
            WebElement ele = (new WebDriverWait(webDriver, 10))
                    .until(ExpectedConditions.presenceOfElementLocated(bytagSelector));
            if (ele != null) {
                // log.infoln(ele.getText());
                if (ele.getAttribute(attribute).equals(value)) {
                    return true;
                }
            }

        } catch (Exception e) {
            log.info("\n" + value + " not found");
            return false;
        }
        return false;

    }

    public boolean checkAttributeValue(By byIdString, String attribute, String value) {
        try {
            WebElement ele = (new WebDriverWait(webDriver, 10))
                    .until(ExpectedConditions.presenceOfElementLocated(byIdString));
            if (ele != null) {
                // log.infoln(ele.getText());
                if (ele.getAttribute(attribute).equalsIgnoreCase(value)) {
                    return true;
                }
            }

        } catch (Exception e) {
            log.info("\n" + value + " not found");
            return false;
        }
        return false;

    }


    public void wait(int time) {
        try {
            Thread.sleep(time);
        } catch (Exception e) {
            log.info("wait interrupted");
        }
    }

    public boolean invisibility(By byId) {
        return (new WebDriverWait(webDriver, 40))
                .until(ExpectedConditions.invisibilityOfElementLocated(byId));

    }

    public boolean checkImageSource(String s, String s1) {
        By byId = By.cssSelector(s);
        WebElement element = (new WebDriverWait(webDriver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(byId));
        return element.getAttribute("src").equals(s1);
    }


    public boolean checkDefaultImageSource(String s, String s1) {
        By byId = By.cssSelector(s);
        WebElement element = (new WebDriverWait(webDriver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(byId));
        return element.getAttribute("style").contains(s1);
    }

    public boolean presence(By byId, int timeout) {
        try {
            WebElement ele = (new WebDriverWait(webDriver, timeout))
                    .until(ExpectedConditions.presenceOfElementLocated(byId));
            if (ele != null) {
                return true;
            }

        } catch (Exception e) {
            log.info("Element info not found");
            return false;
        }
        return false;
    }

    public boolean enterText(By byId, String name) {
        try {
            WebElement ele = (new WebDriverWait(webDriver, 10))
                    .until(ExpectedConditions.visibilityOfElementLocated(byId));
            if (ele != null) {
                ele.clear();
                ele.sendKeys(name);
                return true;
            }
        } catch (Exception e) {
            log.info("Element info not found");
            return false;
        }
        return false;
    }

    public boolean checkDefaultTextInSelectionInput(By byId, String text) {
        Select ele = new Select((new WebDriverWait(webDriver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(byId)));
        return ele.getFirstSelectedOption().getText().equals(text);
    }

    public boolean checkBoxDefaultState(By byId) {
        WebElement ele = (new WebDriverWait(webDriver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(byId));
        return ele.isSelected();
    }

    public boolean presentInPageSource(String s) {
        return webDriver.getPageSource().contains(s);

    }

    public String getText(By byId) {
        WebElement ele = (new WebDriverWait(webDriver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(byId));
        return ele.getText();
    }
}
