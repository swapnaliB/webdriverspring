package com.automation.demo.Pages;

import com.automation.demo.helpers.DomainHelper;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FrontPage {

    @Autowired
    private WebDriver webDriver;
    public void goToHomePage(String google) {
        log.info("rendering the web page");
        log.info(DomainHelper.getDomainName("dev"));
        webDriver.get("http://www.google.com");
    }
}
